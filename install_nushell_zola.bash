#!/bin/bash

set -e

version_pattern="[0-9]+\.[0-9]+\.[0-9]+"
arch=$(uname -m)

# Check if the script is being run as root
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root."
    exit 1
fi

# Update package list
apt update; apt upgrade -y
# Clean up dependencies
apt autoremove --purge -y


# Nu installation
version=$(git -c 'versionsort.suffix=-' ls-remote --tags --sort='-v:refname' http://github.com/nushell/nushell.git | cut --delimiter="/" --fields=3 | grep -Eo "$version_pattern" | head -n1)
echo "Latest Nu shell version is ${version}\n"

nushell_dl_url="https://github.com/nushell/nushell/releases/download/${version}/nu-${version}-${arch}-linux-gnu-full.tar.gz"
nushell_pkg="nu-${version}-${arch}-linux-gnu-full.tar.gz"
echo "Nu shell download url is: ${nushell_dl_url}\n"
echo "Nu shell package name is: ${nushell_pkg}\n"

# Download the latest nushell binary

cd /usr/local/bin/
echo "Downloading the latest nushell archive..."
wget -q --no-check-certificate --show-progress $nushell_dl_url
tar xf $nushell_pkg
echo -e "\nNushell binary has been installed to /usr/local/bin/"


# Install Zola ssg
cd /usr/local/bin/
zola_pkg="zola-v0.18.0-x86_64-unknown-linux-gnu.tar.gz"
zola_dl_url="https://github.com/getzola/zola/releases/download/v0.18.0/zola-v0.18.0-x86_64-unknown-linux-gnu.tar.gz"
echo "Downloading the latest Zola archive..."
wget -q --no-check-certificate --show-progress $zola_dl_url
tar xf $zola_pkg
echo -e "\nZola binary installed inside /usr/local/bin/"

# nushell needs to be renamed to nu
mv $nushell_pkg nu
rm $nushell_pkg
rm $zola_pkg

echo "Nu shell and Zola SSG successfully installed\n"
