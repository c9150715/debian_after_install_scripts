#!/bin/bash

# Extract current architecture
arch=$(uname -m)
# Regex pattern to match version numbers
version_pattern="[0-9]+\.[0-9]+(\.[0-9]+)*"


# List of packages I want to install
critical_packages=(
        ufw
        jq
        git
        xclip
        xournal
        wget
        curl
        ffmpeg
        vlc
        rofi
        signal-desktop
        sudo
        texlive-base
        texlive-binaries
        texlive-fonts-extra
        texlive-fonts-extra-links
        texlive-fonts-recommended
        texlive-latex-base
        texlive-latex-extra
        texlive-latex-recommended
        texlive-pictures
        texlive-plain-generic
        texlive-xetex
        build-essential
        libsqlite3-dev
        sqlite3
        bzip2
        libbz2-dev
        zlib1g-dev
        libssl-dev
        openssl
        libgdbm-dev
        liblzma-dev
        libreadline-dev
        libncursesw5-dev
        libffi-dev
        uuid-dev
        libxml2-dev
        libxslt1-dev
        incus/bookworm-backports
        manpages-dev
        font-manager
        podman
        tmux
        fzf
)

# Check if the script is being run as root
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root."
    exit 1
fi

# Update package list
apt update
# Clean up dependencies
apt autoremove --purge -y
# Clear package cache
apt clean


# Update APT cache
sudo apt update
# Install necessary packages
apt install -y "${critical_packages[@]}"

