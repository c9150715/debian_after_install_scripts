#!/bin/bash

# enable natural scrolling
# edit libinput conf file and insert an option after a specific line

sed -i '/MatchIsPointer "on"/a\ \ \ \ \ \ \ \ Option "NaturalScrolling" "true"' /usr/share/X11/xorg.conf.d/40-libinput.conf