#!/bin/bash

# add a specific option to set caps as ctrl key

sed -i 's/XKBOPTIONS=""/XKBOPTIONS="ctrl:nocaps/g' /etc/default/keyboard