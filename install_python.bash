#!/bin/bash

# Python installation
# Temporary directory for downloading and extracting Python
temp_dir=$(mktemp -d)
cd $temp_dir
version_pattern="3\.1[0-9](\.[0-9]+)*"
version=$(curl -s https://www.python.org/ftp/python/ | grep -Eo $version_pattern | sort -nr | head -n1)
# let me just force here
version="3.12.2"
python_dl_url="https://www.python.org/ftp/python/${version}/Python-${version}.tar.xz"
curl -O -s -S "$python_dl_url"
# Check if curl was successful
if [ $? -eq 0 ]; then
    echo "Download successful."
else
    echo "Download failed."
fi
# Extract Python archive
echo "Extracting Python archive..."
tar -xf "$temp_dir"/Python*.tar.xz -C "$temp_dir"
if [ $? -ne 0 ]; then
    echo "Failed to extract Python archive."
    exit 1
fi
# Move into Python directory
cd "Python*" || exit

# Configure and install Python
echo "Configuring and installing Python..."
./configure
make
sudo make install
# check if install successful
if [ $? -ne 0 ]; then
    echo "Failed to install Python."
    exit 1
fi
echo "Python installation completed."
# Cleanup
rm -rf "$temp_dir"
