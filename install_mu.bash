#!/usr/env/bin bash

set -e

# Check if the script is being run as root
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root."
    exit 1
fi

echo "Installing mu dependencies..."
apt-get update
apt-get autoremove
apt-get upgrade -y
apt install -y meson libgmime-3.0-dev libxapian-dev offlineimap3

# Temporary directory for downloading and extracting mu
temp_dir=$(mktemp -d)
cd $temp_dir

echo "Cloning my repository...."
git clone https://github.com/djcb/mu.git
cd mu
echo "Installing mu...."
./autogen.sh
make
make install
cd ..
rm -rf mu
echo "Mu successfully installed."
