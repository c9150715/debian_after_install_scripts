#!/bin/bash

# Exit immediately if a command exits with a non-zero status
set -e

# Detect OS and set package manager and dependencies
if [[ -f /etc/debian_version ]]; then
    PKG_MANAGER="apt-get"
    UPDATE_CMD="apt-get update"
    INSTALL_CMD="apt-get install -y --no-install-recommends"
elif [[ -f /etc/fedora-release ]]; then
    PKG_MANAGER="dnf"
    UPDATE_CMD="dnf check-update"
    INSTALL_CMD="dnf install -y"
else
    echo "Unsupported operating system."
    exit 1
fi

# Set fonts directory
fonts_dir="/usr/share/fonts"

# Ensure package 'wget' and 'unzip' for downloading and extracting are available
install_dependencies() {
    echo "Installing Emacs dependencies..."
    sudo $UPDATE_CMD
    sudo $INSTALL_CMD wget unzip
}

# Download and extract a font from a given URL
fetch_and_extract_font() {
    local url=$1
    local filename=$(basename "$url")

    echo "Downloading font from $url..."
    wget -q --no-check-certificate --show-progress -P "$tmp_dir" "$url"

    if [[ "$filename" == *.zip ]]; then
        echo "Extracting $filename..."
        unzip -q "$tmp_dir/$filename" -d "$fonts_dir"
        rm "$tmp_dir/$filename"
    else
        mv "$tmp_dir/$filename" "$fonts_dir"
    fi
}

# Reset font cache
reset_font_cache() {
    if command -v fc-cache &>/dev/null; then
        echo -e "\nResetting font cache, this may take a moment..."
        fc-cache -f -v "$fonts_dir"
    else
        echo "Font cache utility not found."
    fi
}

# Main installation process
main() {
    install_dependencies

    mkdir -p "$fonts_dir" "$tmp_dir"

    local font_urls=(
        "https://github.com/tonsky/FiraCode/releases/download/6.2/Fira_Code_v6.2.zip"
        "http://sourceforge.net/projects/dejavu/files/dejavu/2.37/dejavu-fonts-ttf-2.37.zip"
        "https://github.com/source-foundry/Hack/releases/download/v3.003/Hack-v3.003-ttf.zip"
        "https://github.com/microsoft/cascadia-code/releases/download/v2111.01/CascadiaCode-2111.01.zip"
        "https://download.jetbrains.com/fonts/JetBrainsMono-2.304.zip"
        "https://www.marksimonson.com/assets/content/fonts/AnonymousPro-1_002.zip"
        "https://github.com/googlefonts/Inconsolata/releases/download/v3.000/Inconsolata-VF.ttf"
        "https://fontlibrary.org/assets/downloads/fantasque-sans-mono/99bd6bf2755c7f6b5f5c62daf6807aea/fantasque-sans-mono.zip"
    )

    for url in "${font_urls[@]}"; do
        fetch_and_extract_font "$url"
    done

    reset_font_cache

    echo -e "\nAll fonts have been installed to $fonts_dir"
}

# Create a temporary directory
tmp_dir=$(mktemp -d)

# Trap to clean up temporary directory on any exit
trap "rm -rf $tmp_dir" EXIT

# Run the main function
main
