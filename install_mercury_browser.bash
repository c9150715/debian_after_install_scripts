#!/bin/bash

# Check if the script is being run as root
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root."
    exit 1
fi

set -e 
# command at the beginning of your bash script. 
# stop execution if any command returns a non-zero exit status


# Python installation
# Temporary directory for downloading and extracting Python
temp_dir=$(mktemp -d)
cd $temp_dir

# download mercury
version="123.0.1"
mercury_dl_url="https://github.com/Alex313031/Mercury/releases/download/v.123.0.1/mercury-browser_123.0.1_AVX2.deb"
curl -O -s -S "$mercury_dl_url"

# Check if curl was successful
if [ $? -eq 0 ]; then
    echo "Mercury browser download successful."
fi

# install 
dpkg -i "$temp_dir/mercury-browser_123.0.1_AVX2.deb"

# apt install missing deps
apt-get install -f

# reinstall mercury
dpkg -i "$temp_dir/mercury-browser_123.0.1_AVX2.deb"

