#!/bin/bash

# Exit immediately if a command exits with a non-zero status
set -e

# Ensure script is run as root
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root."
    exit 1
fi

# Update library path
export LD_LIBRARY_PATH=/usr/local/lib/

# Detect OS and set package manager and dependencies
if [[ -f /etc/debian_version ]]; then
    PKG_MANAGER="apt-get"
    UPDATE_CMD="apt-get update"
    INSTALL_CMD="apt-get install -y --no-install-recommends"
    DEPS=(
        git autoconf build-essential texinfo libsqlite3-dev gnutls-bin
        libgccjit-12-dev libgtk2.0-dev libgnutls28-dev libxpm-dev libgif-dev
        libgtk-4-dev libncurses-dev libmagick++-dev libgtk-3-dev
        libwebkit2gtk-4.0-dev libgccjit-13-dev libgccjit0
    )
elif [[ -f /etc/fedora-release ]]; then
    PKG_MANAGER="dnf"
    UPDATE_CMD="dnf check-update"
    INSTALL_CMD="dnf install -y"
    DEPS=(
        git autoconf make automake texinfo sqlite-devel gnutls-devel
        gcc gcc-c++ gtk2-devel gnutls-devel libXpm-devel giflib-devel
        gtk4-devel ncurses-devel ImageMagick-c++-devel gtk3-devel
        webkit2gtk3-devel gcc12 libgccjit libgccjit-devel
    )
else
    echo "Unsupported operating system."
    exit 1
fi

# Create a temporary directory
temp_dir=$(mktemp -d)
trap 'rm -rf "$temp_dir"' EXIT

# Function to install dependencies
install_dependencies() {
    echo "Installing Emacs dependencies..."
    sudo $UPDATE_CMD
    sudo $PKG_MANAGER autoremove -y
    sudo $INSTALL_CMD "${DEPS[@]}"
}

# Function to install tree-sitter
install_tree_sitter() {
    echo "Installing tree-sitter..."
    git clone https://github.com/tree-sitter/tree-sitter.git "$temp_dir/tree-sitter"
    pushd "$temp_dir/tree-sitter"
    make
    make install
    popd
}

# Function to download and compile Emacs
install_emacs() {
    echo "Downloading and compiling Emacs..."
    git clone -b master git://git.sv.gnu.org/emacs.git "$temp_dir/emacs"
    pushd "$temp_dir/emacs"
    ./autogen.sh
    ./configure --with-tree-sitter --with-native-compilation --with-mailutils --with-gnu-tls \
      --with-pgtk --with-sqlite3 --with-imagemagick --with-x-toolkit=lucid \
      --with-json --with-xwidgets --with-jpeg --with-png --with-rsvg --with-tiff \
      --with-wide-int --with-xft --with-xml2 --with-dbus --with-harfbuzz \
      --with-compress-install --with-zlib
    make
    make install
    popd
}

# Main script execution
install_dependencies
install_tree_sitter
install_emacs

echo "Emacs installation completed successfully."
